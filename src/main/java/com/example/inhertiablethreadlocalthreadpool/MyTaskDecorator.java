package com.example.inhertiablethreadlocalthreadpool;

import org.springframework.core.task.TaskDecorator;

public class MyTaskDecorator implements TaskDecorator {
    @Override
    public Runnable decorate(Runnable runnable) {
        String user = MyContext.user.get();
        return () -> {
            try {
            MyContext.user.set(user);
            runnable.run();
            } finally {
                MyContext.user.remove();
            }
        };
    }
}

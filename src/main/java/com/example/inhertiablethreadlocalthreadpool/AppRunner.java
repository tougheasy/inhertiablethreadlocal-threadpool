package com.example.inhertiablethreadlocalthreadpool;

import com.example.inhertiablethreadlocalthreadpool.service.WorkerService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

@Component
public class AppRunner implements CommandLineRunner {

    private static final Logger logger = LoggerFactory.getLogger(AppRunner.class);

    private final WorkerService workerService;

    public AppRunner(WorkerService workerService) {
        this.workerService = workerService;
    }

    @Override
    public void run(String... args) throws Exception {
        // Start the clock
        long start = System.currentTimeMillis();

        // Kick of multiple, asynchronous lookups
        for (int i = 1; i < 11; i++) {
            MyContext.user.set("User:" +i);
            workerService.doSomeCoding("Task #" + i);
        }

        }

}
package com.example.inhertiablethreadlocalthreadpool;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;

@SpringBootApplication
@EnableAsync
public class InhertiablethreadlocalThreadpoolApplication {

	public static void main(String[] args) {
		SpringApplication.run(InhertiablethreadlocalThreadpoolApplication.class, args);
	}

}

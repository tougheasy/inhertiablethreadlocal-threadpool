package com.example.inhertiablethreadlocalthreadpool.service;

import com.example.inhertiablethreadlocalthreadpool.MyTaskDecorator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.task.TaskExecutor;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

@Configuration
public class ExecutorConfiguration {

    @Bean
    public TaskExecutor patelExecutor() {
        ThreadPoolTaskExecutor executor = new ThreadPoolTaskExecutor();
        executor.setCorePoolSize(3);
        executor.setMaxPoolSize(6);
        executor.setQueueCapacity(500);
        executor.setThreadNamePrefix("Kaamwala-");
        executor.initialize();
        executor.setTaskDecorator(new MyTaskDecorator());
        return executor;
    }

}

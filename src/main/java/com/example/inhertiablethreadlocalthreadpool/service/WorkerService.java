package com.example.inhertiablethreadlocalthreadpool.service;

import com.example.inhertiablethreadlocalthreadpool.MyContext;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

@Service
public class WorkerService {

    @Async
    public void doSomeCoding(String s) {
        System.out.println("Start Thread -" + Thread.currentThread().getName() + " " + s+ " "  + MyContext.user.get());
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("---------------------->End Thread -" + Thread.currentThread().getName() + " " + s + " "  + MyContext.user.get());

    }
}
